<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Widow</title>
	<style>
		img{
			height: auto;
			max-width: 100%;
		}
	</style>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<ul class="list-inline">
	<li class="list-group-item"><a href="list_blog_with_sidebar.php">List blog with sidebar</a></li>
	<li class="list-group-item"><a href="list_blog_without_sidebar.php">List blog without sidebar</a></li>
	<li class="list-group-item"><a href="single_blog_with_sidebar.php">Single blog with sidebar</a></li>
	<li class="list-group-item"><a href="masonry_blog.php">Masonry blog</a></li>
	<li class="list-group-item"><a href="portfolio_single.php">Portfolio single</a></li>
	<li class="list-group-item"><a href="portfolio.php">Portfolio</a></li>
	<li class="list-group-item"><a href="elements.php">Elements</a></li>
	<li class="list-group-item"><a href="home_v_1.php">Home v 1</a></li>
	<li class="list-group-item"><a href="contact.php">Contact</a></li>
	<li class="list-group-item"><a href="about_us.php">About us</a></li>
	<li class="list-group-item"><a href="services.php">Services</a></li>
</ul>